/* 
 * File:   openfile.h
 * Author: varlen
 *
 * Created on 22 de Junho de 2016, 13:38
 */

#ifndef OPENFILE_H
#define OPENFILE_H

#ifdef __cplusplus
extern "C" {
#endif

FILE* openFile(char* filename);

#ifdef __cplusplus
}
#endif

#endif /* OPENFILE_H */

