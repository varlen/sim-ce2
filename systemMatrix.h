#ifndef SYSTEMMATRIX_H
#define SYSTEMMATRIX_H

#include "netlistParser.h"

#ifdef __cplusplus
extern "C" {
#endif
// Precisão
#define TOLG 1e-10
#define OPEN_CONDUTANCE 1e-9
#define NOT_CONVERGED  0
#define CONVERGED      1
/**
 * Writes zeros to the entire matrix
 * @param matrix
 */
void matrixInitialize(double matrix[][MAX_NODE+2]);
/**
 * Prints the given matrix
 * @param matrix
 * @param int variableCount
 */
void matrixPrint(double matrix[][MAX_NODE+2], int variableCount);
/**
 * Applies continuos current stamps to the given matrix.
 * @param matrix
 * @param netlist
 * @param netlistSize
 * @param frequency    
 */
void matrixAddDCStamps( double matrix[][MAX_NODE+2], element* netlist, int netlistSize, int variableCount );
/**
 * Linear system resolution using Gauss-Jordan method
 * @param double matrix[][MAX_NODE+2]
 * @param variableCount
 * @return 0 if ok or SINGULAR_SYSTEM if not ok
 */
int matrixSolveDC( double matrix[][MAX_NODE+2],int variableCount );
/**
 * Outputs the solution of a system after calculation
 * 
 * @param matrix
 * @param variableCount
 */
void matrixPrintDCSolution( double matrix[][MAX_NODE+2], char nodeList[][MAX_INT_NODE_NAME], int variableCount );
/**
 * Checks if the given solution is the Newton Raphson convergence solution
 * @param matrix
 * @param lastSolution
 * @param variableCount
 * @return 
 */
int matrixCheckNewtonRaphsonConvergence( double matrix[][MAX_NODE+2], double lastSolution[], int variableCount );
/**
 * Copies the solution column to the given array
 * @param systemMatrix
 * @param target
 * @param variableCount
 */
void copySolution(double matrix[][MAX_NODE+2], double target[], int variableCount );
#ifdef __cplusplus
}
#endif

#endif /* SYSTEMMATRIX_H */

