#ifndef FREQUENCIA_H
#define FREQUENCIA_H

#include <tgmath.h>
#include "netlistParser.h"
#include "mosfetManager.h"
#include "messages.h"

#define MAX_FREQ 5000

#ifdef __cplusplus
extern "C" {
#endif

void freqAnalysis( double matrix[][MAX_NODE+2], int variableCount, element* netlist, char nodeList[][MAX_INT_NODE_NAME], int netlistSize, SimulationConfig options, char filename[MAX_SIZE_FILENAME + 1] );
    /**
     * Generates the frequencies to calculate, in Hz.
     * 
     * @param frequency Array which will store the frequencies
     * @param options SimulationConfig struct
     * @return Number of generated points
     */
    int generateFrequencies(double frequency[MAX_FREQ], SimulationConfig options );
    void addOutputFileHeader(FILE* outputFile, char nodeList[][MAX_INT_NODE_NAME], int variableCount );
    void generateOutputFileName(char inputName[MAX_SIZE_FILENAME + 1], char strBuf[MAX_SIZE_FILENAME + 1]);
    void acStoreResults(FILE* outputFile, double freq, double complex acSys[][MAX_NODE+2], int variableCount);
    void acmatrixInitialize(double complex acmatrix[][MAX_NODE+2]);
    void acmatrixPrint(double complex acmatrix[][MAX_NODE+2], int variableCount);
    void acmatrixAddStamps(double complex matrix[][MAX_NODE+2], int netlistSize, element* netlist, int variableCount, double angularFreq );
    int acmatrixSolve(double complex acmatrix[][MAX_NODE+2], int variableCount);
#ifdef __cplusplus
}
#endif

#endif /* FREQUENCIA_H */

