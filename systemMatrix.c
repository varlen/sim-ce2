#include <math.h>

#include "error.h"
#include "mosfet.h"
#include "systemMatrix.h"
#include "messages.h"

void matrixInitialize(double matrix[][MAX_NODE+2]) {
    int y;
    for (int x = 0; x <= MAX_NODE; x++ ) {
        for (y = 0; y <= MAX_NODE + 1; y++) {
            matrix[x][y] = 0;
        }
    }
};

void matrixPrint( double matrix[][MAX_NODE+2], int variableCount ) {
    int y; 
    char line[MAX_LINE_SIZE];
    //char valueStr[8];
    for (int x = 1; x <= variableCount; x++ ) {
        line[0] = '\0';
        for (y = 1; y <= variableCount + 1; y++) {
            if ( matrix[x][y] != 0 ) printf("%+.2e", matrix[x][y]);//sprintf(valueStr, "%.10e", matrix[x][y]);
            else printf(".........");//strcpy(valueStr, "......");
//            strcat(line, valueStr);
//            if ( y != variableCount + 1 ) {
                //strcat(line," ");
                printf(" ");
//            }
        }
        printf("\n");
        //consolePrint(line);
    }
}

void matrixAddDCStamps(double matrix[][MAX_NODE+2], element* netlist, int netlistSize, int variableCount) {
    double g;
    char strRasc[MAX_LINE_SIZE];
    for ( int elementIndex = 1; elementIndex <= netlistSize; elementIndex++ ) {
        switch( netlist[elementIndex].type ) {
            case 'R':
                g = 1/netlist[elementIndex].param[VALUE];
                matrix[netlist[elementIndex].connection[POINT_A]]
                      [netlist[elementIndex].connection[POINT_A]] += g;
                matrix[netlist[elementIndex].connection[POINT_B]]
                      [netlist[elementIndex].connection[POINT_B]] += g;
                matrix[netlist[elementIndex].connection[POINT_A]]
                      [netlist[elementIndex].connection[POINT_B]] -= g;
                matrix[netlist[elementIndex].connection[POINT_B]]
                      [netlist[elementIndex].connection[POINT_A]] -= g;
                break;
            case 'C': 
                g = OPEN_CONDUTANCE;
                matrix[netlist[elementIndex].connection[POINT_A]]
                      [netlist[elementIndex].connection[POINT_A]] += g;
                matrix[netlist[elementIndex].connection[POINT_B]]
                      [netlist[elementIndex].connection[POINT_B]] += g;
                matrix[netlist[elementIndex].connection[POINT_A]]
                      [netlist[elementIndex].connection[POINT_B]] -= g;
                matrix[netlist[elementIndex].connection[POINT_B]]
                      [netlist[elementIndex].connection[POINT_A]] -= g;
                break;
            case 'L': // Indutor modelado como curto em DC
                g = OPEN_CONDUTANCE;
                matrix[netlist[elementIndex].connection[POINT_A]]
                      [netlist[elementIndex].connection[POINT_X]]
                    += 1;
                matrix[netlist[elementIndex].connection[POINT_B]]
                      [netlist[elementIndex].connection[POINT_X]]
                    -= 1;
                matrix[netlist[elementIndex].connection[POINT_X]]
                      [netlist[elementIndex].connection[POINT_A]]
                    -= 1;
                matrix[netlist[elementIndex].connection[POINT_X]]
                      [netlist[elementIndex].connection[POINT_B]]
                    += 1;
                matrix[netlist[elementIndex].connection[POINT_X]]
                      [netlist[elementIndex].connection[POINT_X]]
                    += g;
                break;
            case 'M':
                mosfetAddStampDC( matrix, &netlist[elementIndex], variableCount );
                break;
            case 'G':
                matrix[netlist[elementIndex].connection[POINT_A]]
                      [netlist[elementIndex].connection[POINT_C]] 
                    += netlist[elementIndex].param[VALUE];
                matrix[netlist[elementIndex].connection[POINT_B]]
                      [netlist[elementIndex].connection[POINT_D]] 
                    += netlist[elementIndex].param[VALUE];
                
                matrix[netlist[elementIndex].connection[POINT_A]]
                      [netlist[elementIndex].connection[POINT_D]]
                    -= netlist[elementIndex].param[VALUE];
                matrix[netlist[elementIndex].connection[POINT_B]]
                      [netlist[elementIndex].connection[POINT_C]] 
                    -= netlist[elementIndex].param[VALUE];
                break;
                
            case 'I':
                matrix[netlist[elementIndex].connection[POINT_A]]
                      [variableCount + 1] 
                    -= netlist[elementIndex].param[VALUE];
                matrix[netlist[elementIndex].connection[POINT_B]]
                      [variableCount + 1] 
                    += netlist[elementIndex].param[VALUE];
                break;
                
            case 'V':
                matrix[netlist[elementIndex].connection[POINT_A]]
                      [netlist[elementIndex].connection[POINT_X]]
                    += 1;
                matrix[netlist[elementIndex].connection[POINT_B]]
                      [netlist[elementIndex].connection[POINT_X]]
                    -= 1;
                matrix[netlist[elementIndex].connection[POINT_X]]
                      [netlist[elementIndex].connection[POINT_A]]
                    += 1;
                matrix[netlist[elementIndex].connection[POINT_X]]
                      [netlist[elementIndex].connection[POINT_B]]
                    -= 1;
                matrix[netlist[elementIndex].connection[POINT_X]]
                      [variableCount+1]
                    += netlist[elementIndex].param[VALUE];
                break;
                
            case 'E':
                matrix[netlist[elementIndex].connection[POINT_A]]
                      [netlist[elementIndex].connection[POINT_X]]
                    += 1;
                matrix[netlist[elementIndex].connection[POINT_B]]
                      [netlist[elementIndex].connection[POINT_X]]
                    -= 1;
                matrix[netlist[elementIndex].connection[POINT_X]]
                      [netlist[elementIndex].connection[POINT_A]]
                    -= 1;
                matrix[netlist[elementIndex].connection[POINT_X]]
                      [netlist[elementIndex].connection[POINT_B]]
                    += 1;
                matrix[netlist[elementIndex].connection[POINT_X]]
                      [netlist[elementIndex].connection[POINT_C]]
                    += netlist[elementIndex].param[VALUE];
                matrix[netlist[elementIndex].connection[POINT_X]]
                      [netlist[elementIndex].connection[POINT_D]]
                    -= netlist[elementIndex].param[VALUE];
                break;
            case 'F':
                matrix[netlist[elementIndex].connection[POINT_A]]
                      [netlist[elementIndex].connection[POINT_X]]
                    += netlist[elementIndex].param[VALUE];
                matrix[netlist[elementIndex].connection[POINT_B]]
                      [netlist[elementIndex].connection[POINT_X]]
                    -= netlist[elementIndex].param[VALUE];
                
                matrix[netlist[elementIndex].connection[POINT_C]]
                      [netlist[elementIndex].connection[POINT_X]]
                    += 1;
                matrix[netlist[elementIndex].connection[POINT_D]]
                      [netlist[elementIndex].connection[POINT_X]]
                    -= 1;
                
                matrix[netlist[elementIndex].connection[POINT_X]]
                      [netlist[elementIndex].connection[POINT_C]]
                    -= 1;
                matrix[netlist[elementIndex].connection[POINT_X]]
                      [netlist[elementIndex].connection[POINT_D]]
                    += 1;
                break;
            case 'H':
                matrix[netlist[elementIndex].connection[POINT_A]]
                      [netlist[elementIndex].connection[POINT_Y]] 
                    += 1;
                matrix[netlist[elementIndex].connection[POINT_B]]
                      [netlist[elementIndex].connection[POINT_Y]] 
                    -= 1;
                matrix[netlist[elementIndex].connection[POINT_C]]
                      [netlist[elementIndex].connection[POINT_X]] 
                    += 1;
                matrix[netlist[elementIndex].connection[POINT_D]]
                      [netlist[elementIndex].connection[POINT_X]] 
                    -= 1;
                
                matrix[netlist[elementIndex].connection[POINT_Y]]
                      [netlist[elementIndex].connection[POINT_A]] 
                    -= 1;
                matrix[netlist[elementIndex].connection[POINT_Y]]
                      [netlist[elementIndex].connection[POINT_B]] 
                    += 1;
                matrix[netlist[elementIndex].connection[POINT_X]]
                      [netlist[elementIndex].connection[POINT_C]] 
                    -= 1;
                matrix[netlist[elementIndex].connection[POINT_X]]
                      [netlist[elementIndex].connection[POINT_D]] 
                    += 1;
                
                matrix[netlist[elementIndex].connection[POINT_X]]
                      [netlist[elementIndex].connection[POINT_Y]] 
                    += netlist[elementIndex].param[VALUE];
                break;
            case 'O':
                matrix[netlist[elementIndex].connection[POINT_A]]
                      [netlist[elementIndex].connection[POINT_X]] 
                    += 1;
                matrix[netlist[elementIndex].connection[POINT_B]]
                      [netlist[elementIndex].connection[POINT_X]] 
                    -= 1;
                matrix[netlist[elementIndex].connection[POINT_X]]
                      [netlist[elementIndex].connection[POINT_C]] 
                    += 1;
                matrix[netlist[elementIndex].connection[POINT_X]]
                      [netlist[elementIndex].connection[POINT_D]] 
                    -= 1;
                break;
            case 'K':
                break;
            default:
                sprintf( strRasc, "Sem estampa DC para o elemento %c", netlist[elementIndex].type );
                consolePrint( strRasc );
        }
        //sprintf( strRasc, "Matriz apos elemento %s:", netlist[elementIndex].name );
        //consolePrint( strRasc );
        //matrixPrint(matrix, variableCount);
        //consolePrint("");
    }
    //getchar();
}

int matrixSolveDC( double matrix[][MAX_NODE+2],int variableCount ) {
    int i,j,l,a;
    double t,p;
    for (i=0; i<= variableCount; i++ ) {
        matrix[0][i] = 0.0;
        matrix[i][0] = 0.0;
    }
    matrix[0][variableCount+1] = 0.0;
    
    for (i=1; i <= variableCount; i++ ) {
        t=0.0;
        a=i;
        
        for (l=i; l<= variableCount; l++) {
            if( fabs(matrix[l][i]) > fabs(t) ) {
                a = l;
                t = matrix[l][i];
            }
        }
        
        if ( i != a ) {
            for ( l=1;l<= variableCount + 1; l++ ) {
                p = matrix[i][l];
                matrix[i][l] = matrix[a][l];
                matrix[a][l] = p;
            }
        }
        
        if ( fabs(t) < TOLG ) {
            return SINGULAR_SYSTEM;
        }
        
        for (j= variableCount+1; j>0; j--) {
            matrix[i][j]/= t;
            p = matrix[i][j];
            if ( p != 0 ) {
                for(l=1;l<= variableCount; l++ ) {
                    if ( l != i ) 
                        matrix[l][j] -= matrix[l][i] * p;
                }
            }            
        }
    }
    return 0;
}

void matrixPrintDCSolution(double matrix[][MAX_NODE+2],char nodeList[][MAX_INT_NODE_NAME],int variableCount ) {
    char strRasc[MAX_LINE_SIZE+1];
    consolePrint("--- Solucao ----");
    for (int i=1;i<=variableCount; i++) {
        sprintf(strRasc,"%s: %g", nodeList[i],matrix[i][variableCount+1]);
        consolePrint(strRasc);
    }
    consolePrint("----------------");
}

int matrixCheckNewtonRaphsonConvergence( double matrix[][MAX_NODE+2], double lastSolution[], int variableCount ) {
    int i;
    for ( i = 1; i <= variableCount; i++ ) {
        if ( fabs( matrix[i][variableCount+1] - lastSolution[i] ) > TOLG ) return NOT_CONVERGED;
    }
    return CONVERGED;
}

void copySolution(double matrix[][MAX_NODE+2], double target[], int variableCount) {
    for ( int i = 1; i <= variableCount; i++ ) {
        target[i] = matrix[i][variableCount+1];
    }
}
