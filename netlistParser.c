#include "netlistParser.h"
#include "element.h"
#include "messages.h"
#include "error.h"
#include "mosfet.h"

// Verifica se um nó já existe no netlist lido e retorna o seu valor
int matchToNode(char* nodeName, char nodeList[][MAX_INT_NODE_NAME], int* variableCount) {
    int index = 0;
    int found = 0;
    
    // Nó zero
    if ( nodeName[0] == '0' && nodeName[1] == '\0' ) {
        if ( strcmp( nodeList[0], nodeName ) ) {
            strcpy( nodeList[0], nodeName );
        }
        return 0;
    }
    // Nó diferente de zero
    while (!found && index <= *variableCount ) { 
        found = !strcmp(nodeName, nodeList[index]);
        if (!found) index++;
    }
    if ( !found ) {
        if ( *variableCount == MAX_NODE ) {
            consolePrint("Maximo de nos atingido");
            exit(MAX_NODE_REACHED);
        }
        *variableCount = *variableCount + 1;
        strcpy(nodeList[*variableCount], nodeName );
        return *variableCount; // Novo nó registrado
    } else {
        strcpy(nodeList[index], nodeName);
        return index; // Nó existente
    }
}

int parseNetlist(element* netlist, FILE* arquivo, int* variableCount, int* mosfetCount, int mosfetIndex[MAX_MOSFET], char nodeList[][MAX_INT_NODE_NAME], SimulationConfig* options) {
    char line[MAX_NETLIST_LINE+1];
    char* strRasc;
    char Mtype[5];
    char Mlength[11], Mwidth[11];
    char optScale[4];
    //char nodeList[MAX_NODE+1][MAX_STR_ELEMENT+1];
    char* lineAfterName;
    int index = 0; // TODO - Change name to elementCount
    char elementType;
    char inductorA[MAX_NODE_NAME];
    char inductorB[MAX_NODE_NAME];
    double k_value;
    int indexA, indexB;
    
    consolePrint("Lendo arquivo de netlist...");
    fgets(line,MAX_NETLIST_LINE,arquivo);
    consolePrint(line);
    // Processar linha a linha
    while(fgets(line,MAX_NETLIST_LINE,arquivo)) {
        char na[MAX_NODE_NAME],nb[MAX_NODE_NAME],nc[MAX_NODE_NAME],nd[MAX_NODE_NAME];
        index++;
        // Não usa netlist[0]
        if ( index > MAX_ELEMENTS ) {
            consolePrint("Maximo de elementos na netlist atingido");
            exit(NETLIST_TOO_BIG);
        }
        line[0] = toupper(line[0]);
        elementType = line[0];
        sscanf(line, "%10s", netlist[index].name );

        // 'Corta' o nome do elemento fora da string usando aritmetica de ponteiro
        lineAfterName = line + strlen(netlist[index].name);
        switch( elementType ) {
            case 'R': // Resistor
            case 'L': // Indutor
            case 'C': // Capacitor
                sscanf(lineAfterName, "%10s%10s%lg", na, nb, 
                        &netlist[index].param[VALUE]);
                netlist[index].connection[POINT_A] = matchToNode(na,nodeList, variableCount );
                netlist[index].connection[POINT_B] = matchToNode(nb,nodeList, variableCount );
                break;
            case 'K': // Acoplamento entre indutores
                sscanf(lineAfterName, "%10s%10s%lg", inductorA, inductorB, &k_value);

                indexA = findElementName(netlist, index, inductorA);
                indexB = findElementName(netlist, index, inductorB);

                if ( (indexA < 0) || (indexB < 0) ) {
                    consolePrint("Indutores invalidos");
                    exit(BAD_INDUCTOR);
                }
                else {
                    netlist[index].connection[INDUCTOR_A] = indexA;
                    netlist[index].connection[INDUCTOR_B] = indexB;
                    netlist[index].param[VALUE] = k_value;
                }
                break;
            case 'I': // Fonte de Corrente
            case 'V': // Fonte de Tensão
                netlist[index].param[VALUE] = 0;
                netlist[index].param[PHASE] = 0;
                netlist[index].param[AMPLITUDE] = 0;
                sscanf(lineAfterName, "%10s%10s%lg%lg%lg", na, nb, 
                        &netlist[index].param[AMPLITUDE],
                        &netlist[index].param[PHASE],
                        &netlist[index].param[VALUE]); // DC Value
                netlist[index].connection[POINT_A] = matchToNode(na,nodeList, variableCount );
                netlist[index].connection[POINT_B] = matchToNode(nb,nodeList, variableCount );
                break;
            case 'G':
            case 'E':
            case 'F':
            case 'H':
                sscanf(lineAfterName,"%10s%10s%10s%10s%lg",na,nb,nc,nd,&netlist[index].param[VALUE]);
                netlist[index].connection[POINT_A] = matchToNode(na,nodeList, variableCount );
                netlist[index].connection[POINT_B] = matchToNode(nb,nodeList, variableCount );
                netlist[index].connection[POINT_C] = matchToNode(nc,nodeList, variableCount );
                netlist[index].connection[POINT_D] = matchToNode(nd,nodeList, variableCount );
                break;
            case 'O':
                sscanf(lineAfterName,"%10s%10s%10s%10s",na,nb,nc,nd);
                netlist[index].connection[POINT_A] = matchToNode(na,nodeList, variableCount );
                netlist[index].connection[POINT_B] = matchToNode(nb,nodeList, variableCount );
                netlist[index].connection[POINT_C] = matchToNode(nc,nodeList, variableCount );
                netlist[index].connection[POINT_D] = matchToNode(nd,nodeList, variableCount );
                break;
            case 'M':
                //Transistor MOS: M<nome> <nód> <nóg> <nós> <nób> <NMOS ou PMOS> 
                //L=<comprimento> W=<largura> <K> <Vt0> <lambda> <gamma> <phi> <Ld>
                sscanf(lineAfterName,"%10s%10s%10s%10s%10s%10s%10s%lg%lg%lg%lg%lg%lg",
                        na,nb,nc,nd,Mtype,Mlength,Mwidth,&netlist[index].param[K_PARAM],
                        &netlist[index].param[VTO], &netlist[index].param[LAMBDA],
                        &netlist[index].param[GAMMA], &netlist[index].param[PHI],
                        &netlist[index].param[LD]);
                
                netlist[index].connection[POINT_DRAIN]  = matchToNode(na, nodeList, variableCount );
                netlist[index].connection[POINT_GATE]   = matchToNode(nb, nodeList, variableCount );
                netlist[index].connection[POINT_SOURCE] = matchToNode(nc, nodeList, variableCount );
                netlist[index].connection[POINT_BODY]   = matchToNode(nd, nodeList, variableCount );
                
                if ( !strcmp(Mtype,"PMOS") ) {
                    netlist[index].param[POLARITY] = PMOS;
                } else if ( !strcmp(Mtype, "NMOS") ) {
                    netlist[index].param[POLARITY] = NMOS;
                } else {
                    consolePrint("Bad MOS Transistor type");
                    exit(BAD_MOS);
                }
                
                strRasc = Mlength;
                if ( Mlength[0] == 'L' && Mlength[1] == '=') 
                    strRasc = strRasc + 2; // Jump L=
                netlist[index].param[LENGTH] = atof(strRasc);
                
                strRasc = Mwidth;
                if ( Mwidth[0] == 'W' && Mwidth[1] == '=')
                    strRasc = strRasc + 2; // Jump W=
                netlist[index].param[WIDTH] = atof(strRasc);
                
                netlist[index].param[KWL] = netlist[index].param[K_PARAM] 
                        * (netlist[index].param[WIDTH]/netlist[index].param[LENGTH]);
                
                netlist[index].param[STATE] = POE_UNKNOWN;
                netlist[index].param[V_GATE] = 0.0;
                netlist[index].param[V_DRAIN] = 0.0;
                netlist[index].param[V_BODY] = 0.0;
                netlist[index].param[V_SOURCE] = 0.0;
                mosfetIndex[*mosfetCount] = index;
                *mosfetCount = *mosfetCount + 1;
                if ( *mosfetCount == MAX_MOSFET ) {
                    consolePrint("Maximo de MOSFETs atingido!!!");
                }
                break;
            case '*': // Comentário
                consolePrint(line);
                index--;
                continue;
            case '.': // Opção
                consolePrint(line);
                if ( line[1] == 'A' && line[2] == 'C' ) {
                    sscanf(lineAfterName, "%s %d %lg %lg", optScale, &options->points, &options->begin, &options->end  );
                    if ( !strcmp(optScale, "DEC") ) {
                        options->scale = DEC;
                    } else if ( !strcmp(optScale, "LIN") ) {
                        options->scale = LIN;
                    } else if ( !strcmp(optScale, "OCT") ) {
                        options->scale = OCT;
                    } else {
                        consolePrint("Escala invalida na opcao .AC - Usando escala padrao (DEC)");
                    }
                }
                index--;
                continue;
            default:
                consolePrint("Elemento desconhecido:");
                consolePrint(line);
                return UNKNOWN_ELEMENT;
        }
        netlist[index].type = elementType;
    }
    return index;
}

int setExtraCurrents(element* netlist,int netlistSize,int* variableCount, char nodeList[][MAX_INT_NODE_NAME]){
    int extraCurrents = 0;
    int newVariablePosition;
    
    for (int index = 0; index <= netlistSize; index++ ) {
        switch( netlist[index].type ) {
            case 'V':
            case 'E':
            case 'F':
            case 'L':
            case 'O':
                extraCurrents++;
                newVariablePosition = extraCurrents + *variableCount;
                if ( newVariablePosition > MAX_NODE ) return MAX_NODE_REACHED;
                strcpy(nodeList[newVariablePosition], "j");
                strcat(nodeList[newVariablePosition], netlist[index].name );
                netlist[index].connection[POINT_X] = newVariablePosition;
                break;
            case 'H':
                extraCurrents = extraCurrents + 2;
                newVariablePosition = extraCurrents + *variableCount;
                if ( newVariablePosition > MAX_NODE ) return MAX_NODE_REACHED;
                strcpy(nodeList[newVariablePosition - 1], "jx");
                strcat(nodeList[newVariablePosition - 1], netlist[index].name );
                netlist[index].connection[POINT_X] = newVariablePosition - 1;
                netlist[index].connection[POINT_Y] = newVariablePosition;
                strcpy(nodeList[newVariablePosition], "jy");
                strcat(nodeList[newVariablePosition], netlist[index].name );
                break;
        }
    }
        
    return extraCurrents;
}

int findElementName(element* netlist,int netlistSize, char* elementName) {
    int index = 0;
    int found = 0;
    
    while ( !found && index <= netlistSize ) {
        found = !strcmp(netlist[index].name, elementName);
        if ( !found ) 
            index++;
        else
            return index;
    }
    return ELEMENT_NOT_FOUND;    
}

int countElementType(element* netlist, int netlistSize, char elementType) {
    int index = 0;
    int found = 0;
    int elementsNumber = 0;
    
    while ( !found && index <= netlistSize ) {
        found = !(netlist[index].type == elementType);
        if ( !found )
            index++;
        else
            elementsNumber++;
    }
    return elementsNumber;
}