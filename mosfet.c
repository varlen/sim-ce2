#include <signal.h>
#include <tgmath.h>

#include "error.h"

#include "mosfet.h"
#include "systemMatrix.h"
#include "frequencyAnalysis.h"
#include "error.h"
#include "messages.h"

int mosfetCount( element* netlist, int netlistSize ) {
    int i, count = 0;
    for (i = 0; i < netlistSize; i++ ) 
        if ( netlist[i].type == 'M' ) count++;
    return count;
}

void mosfetReversePolarity(element* mosfet) {
    int aux;
    aux = mosfet->connection[POINT_DRAIN];
    mosfet->connection[POINT_DRAIN] = mosfet->connection[POINT_SOURCE];
    mosfet->connection[POINT_SOURCE] = aux;    
}

void mosfetAddStampAC( double complex matrix[][MAX_NODE+2], element* mosfet, int variableCount, double angularFreq ){
    //mosfetDebug(mosfet);
    element model[7]; // 3 capacitancias +  1 resistencia + 2 I controladas
    
    // Cgs
    model[1].type = 'C';
    strcpy(model[1].name, "Cgs");
    strcat(model[1].name, mosfet->name);
    model[1].param[VALUE] = mosfet->param[CGS];
    model[1].connection[POINT_A] = mosfet->connection[POINT_GATE];
    model[1].connection[POINT_B] = mosfet->connection[POINT_SOURCE];
    
    // Cgd
    model[2].type = 'C';
    strcpy(model[2].name, "Cgd");
    strcat(model[2].name, mosfet->name);
    model[2].param[VALUE] = mosfet->param[CGD];
    model[2].connection[POINT_A] = mosfet->connection[POINT_GATE];
    model[2].connection[POINT_B] = mosfet->connection[POINT_DRAIN];
    
    // Cbg
    model[3].type = 'C';
    strcpy(model[3].name, "Cbg");
    strcat(model[3].name, mosfet->name);
    model[3].param[VALUE] = mosfet->param[CGB];
    model[3].connection[POINT_A] = mosfet->connection[POINT_BODY];
    model[3].connection[POINT_B] = mosfet->connection[POINT_GATE];
    
    // Rds
    model[4].type = 'R';
    strcpy(model[4].name, "Rds");
    strcat(model[4].name, mosfet->name);
    model[4].param[VALUE] = 1/mosfetGetGdsn(mosfet);
    model[4].connection[POINT_A] = mosfet->connection[POINT_DRAIN];
    model[4].connection[POINT_B] = mosfet->connection[POINT_SOURCE];
    
    // GmVgs
    model[5].type = 'G';
    strcpy(model[5].name, "GmVgs");
    strcat(model[5].name, mosfet->name);
    //Gm
    model[5].param[VALUE] = mosfetGetGm(mosfet);
    //I+ I-
    model[5].connection[POINT_A] = mosfet->connection[POINT_SOURCE];
    model[5].connection[POINT_B] = mosfet->connection[POINT_DRAIN];
    //v+ v-
    model[5].connection[POINT_C] = mosfet->connection[POINT_SOURCE];
    model[5].connection[POINT_D] = mosfet->connection[POINT_GATE];
    
    // GmVbs
    model[6].type = 'G';
    strcpy(model[6].name, "GmVbs");
    strcat(model[6].name, mosfet->name);
    //Gm
    model[6].param[VALUE] = mosfetGetGmb(mosfet);
    //I+ I-
    model[6].connection[POINT_A] = mosfet->connection[POINT_SOURCE];
    model[6].connection[POINT_B] = mosfet->connection[POINT_DRAIN];
    //v+ v-
    model[6].connection[POINT_C] = mosfet->connection[POINT_SOURCE];
    model[6].connection[POINT_D] = mosfet->connection[POINT_BODY];
    
    acmatrixAddStamps( matrix, 6, model, variableCount, angularFreq );
}

void mosfetAddStampDC(double matrix[][MAX_NODE+2], element* mosfet, int variableCount) {
    // REESCREVER
    if ( mosfet->param[STATE] == POE_CUT ) {
#ifdef PRINT_DEBUG
        printf("%s - Adicionando a estampa do Corte( state: %f ) \n", mosfet->name, mosfet->param[STATE]);
#endif
        mosfetAddStampCutDC(matrix,mosfet,variableCount);
    } 
    if ( mosfet->param[STATE] == POE_SAT || mosfet->param[STATE] == POE_TRI ) {
#ifdef PRINT_DEBUG
        printf("%s - Adicionando a estampa da Saturacao/Triodo ( state: %f ) \n", mosfet->name, mosfet->param[STATE]);
#endif
        mosfetAddStampFullDC(matrix,mosfet, variableCount);
    }
}

void mosfetAddStampCutDC(double matrix[][MAX_NODE+2], element* mosfet, int variableCount) {
    
    element model[2]; // Netlist starts at index 1, model[0] is not used (gambiarra)
    model[RDS].type = 'R';
    strcpy(model[RDS].name, "Rds");
    strcat(model[RDS].name, mosfet->name );
    model[RDS].param[VALUE] = MOS_CUTOFF_RESISTANCE;
    model[RDS].connection[POINT_A] = mosfet->connection[POINT_DRAIN];
    model[RDS].connection[POINT_B] = mosfet->connection[POINT_SOURCE];
    matrixAddDCStamps(matrix, model, 1, variableCount); // variableCount não importa nesse caso
}

void mosfetAddStampFullDC(double matrix[][MAX_NODE+2], element* mosfet, int variableCount) {
    element model[5];
    double gdsn;
    //-------- Gds
    model[RDS].type = 'R';
    strcpy(model[RDS].name, "Gds");
    strcat(model[RDS].name, mosfet->name );
    gdsn = mosfetGetGdsn(mosfet);
    
    model[RDS].param[VALUE] = 1/gdsn;
    model[RDS].connection[POINT_A] = mosfet->connection[POINT_DRAIN];
    model[RDS].connection[POINT_B] = mosfet->connection[POINT_SOURCE];
    
    //-------- GmVgs                                   A      B      C      D     Val
    // Fonte de corrente controlada a tensão: G<nome> <nóI+> <nóI-> <nóv+> <nóv-> <Gm>
    model[GMVGS].type = 'G';
    strcpy(model[GMVGS].name, "Ggs");
    strcat(model[GMVGS].name, mosfet->name );
    
    model[GMVGS].param[VALUE] = mosfetGetGm(mosfet);
    
    // Inverte o sentido da corrente se for PMOS ( Inverter conexao? )
//    if ( mosfet->param[POLARITY] == PMOS )    
//        model[GMVGS].param[VALUE] = model[GMVGS].param[VALUE] * -1;
    
    model[GMVGS].connection[POINT_A] = mosfet->connection[POINT_SOURCE];
    model[GMVGS].connection[POINT_B] = mosfet->connection[POINT_DRAIN];
    model[GMVGS].connection[POINT_C] = mosfet->connection[POINT_SOURCE];
    model[GMVGS].connection[POINT_D] = mosfet->connection[POINT_GATE];
    
    //------- GmbVbs
    model[GMVBS].type = 'G';
    strcpy(model[GMVBS].name, "Gmb");
    strcat(model[GMVBS].name, mosfet->name );
    
    model[GMVBS].param[VALUE] = mosfetGetGmb(mosfet);
    
    // Inverte o sentido da corrente se for PMOS ( Inverter conexao? )
//    if ( mosfet->param[POLARITY] == PMOS ) 
//        model[GMVBS].param[VALUE] = model[GMVBS].param[VALUE] * -1;
    
    model[GMVBS].connection[POINT_A] = mosfet->connection[POINT_SOURCE];
    model[GMVBS].connection[POINT_B] = mosfet->connection[POINT_DRAIN];
    model[GMVBS].connection[POINT_C] = mosfet->connection[POINT_SOURCE];
    model[GMVBS].connection[POINT_D] = mosfet->connection[POINT_BODY];
    //--------- I0
    model[I_ZERO].type = 'I';
    model[I_ZERO].param[VALUE] = 
        mosfetGetId(mosfet) 
      - (mosfetGetVds(mosfet)/model[RDS].param[VALUE])
      - (mosfetGetVgs(mosfet)*model[GMVGS].param[VALUE])
      - (mosfetGetVbs(mosfet)*model[GMVBS].param[VALUE]);
   
    strcpy(model[I_ZERO].name, "I0");
    strcat(model[I_ZERO].name, mosfet->name );
    
    model[I_ZERO].connection[POINT_A] = mosfet->connection[POINT_DRAIN];
    model[I_ZERO].connection[POINT_B] = mosfet->connection[POINT_SOURCE];
    
    matrixAddDCStamps(matrix, model, 4, variableCount);
}

double mosfetGetVds(element* mosfet) {
    return mosfet->param[V_DRAIN] - mosfet->param[V_SOURCE];
}

double mosfetGetVgs(element* mosfet) {
    return mosfet->param[V_GATE] - mosfet->param[V_SOURCE];
}

double mosfetGetVbs(element* mosfet) {
    return mosfet->param[V_BODY] - mosfet->param[V_SOURCE];
}

double mosfetGetGdsn(element* mosfet) {
    double Vds, Vgs, Vt, VgsVt, Gdsn;
    
    if ( mosfet->param[STATE] == POE_CUT ) {
        //return MOS_CUTOFF_CONDUTANCE;
        return 0;
    }
    
    Vgs = mosfetGetVgs( mosfet );
    Vt = mosfetGetVt( mosfet );
    if ( mosfet->param[POLARITY] == NMOS ){
        VgsVt = Vgs - Vt;
        Vds = mosfetGetVds( mosfet );
    }        
    else {
        VgsVt = (-Vgs) - Vt;
        Vds = -mosfetGetVds( mosfet );
    }
   
    if ( mosfet->param[STATE] == POE_SAT ) {
        Gdsn = ( mosfet->param[KWL] * ( VgsVt * VgsVt ) * mosfet->param[LAMBDA] );
    }
    if ( mosfet->param[STATE] == POE_TRI ) {
        Gdsn = (mosfet->param[KWL] * ( 
                (2 * VgsVt) - (2 * Vds) 
                + (4 * mosfet->param[LAMBDA] * Vds * VgsVt) 
                - (3 * mosfet->param[LAMBDA] * Vds * Vds) 
                ));
    }
    return Gdsn;
}

double mosfetGetGm(element* mosfet) {
    double Vds, Vgs, Vt, VgsVt, Gm;
    
    if ( mosfet->param[STATE] == POE_CUT ) {
        //Gm = MOS_CUTOFF_CONDUTANCE;
        return 0;
    }
    
    Vgs = mosfetGetVgs( mosfet );
    Vt = mosfetGetVt( mosfet );
    if ( mosfet->param[POLARITY] == NMOS ){
        VgsVt = Vgs - Vt;
        Vds = mosfetGetVds( mosfet );
    }        
    else {
        VgsVt = (-Vgs) - Vt;
        Vds = -mosfetGetVds( mosfet );
    }
        
    if ( mosfet->param[STATE] == POE_TRI ) {
        Gm = mosfet->param[KWL] * (2 * Vds * ( 1 + mosfet->param[LAMBDA] * Vds));
    }
    if ( mosfet->param[STATE] == POE_SAT ) {
        Gm = mosfet->param[KWL] * (2 * VgsVt * ( 1 + mosfet->param[LAMBDA] * Vds));
    }
    return Gm;
}

double mosfetGetGmb(element* mosfet) {
    double Gm, Gmb, Vbs, VbsAux;
       
    if ( mosfet->param[STATE] == POE_CUT ) {
        return MOS_CUTOFF_CONDUTANCE;
    }
    
    Gm = mosfetGetGm(mosfet);
    Vbs = mosfetGetVbs( mosfet );
    if ( mosfet->param[POLARITY] == NMOS ){
        if ( Vbs > (mosfet->param[PHI] / 2) )
            VbsAux = mosfet->param[PHI] / 2;
        else
            VbsAux = Vbs;
    }
    else {
        if ( (-Vbs) > (mosfet->param[PHI] / 2) )
            VbsAux = (-mosfet->param[PHI]) / 2;
        else
            VbsAux = Vbs;
    }
    Gmb = ( (Gm * mosfet->param[GAMMA]) / (2 * sqrt(mosfet->param[PHI] - VbsAux)) );
    return Gmb;
}

double mosfetGetVt( element* mosfet ) {
    double Vbs, Vt, VbsAux;
    
    // A polaridade precisa ser ajustada antes desse ponto.
    mosfetUpdatePolarity(mosfet);
    
    Vbs = mosfetGetVbs(mosfet);
    if ( mosfet->param[POLARITY] == NMOS ) {
        if ( Vbs > (mosfet->param[PHI] / 2) )
            VbsAux = mosfet->param[PHI] / 2;
        else
            VbsAux = Vbs;               
    }
    else {
        if ( -Vbs > (mosfet->param[PHI] / 2) )
            VbsAux = -mosfet->param[PHI] / 2;
        else
            VbsAux = Vbs;
    }
    Vt = mosfet->param[VTO] + mosfet->param[GAMMA] 
         * ( sqrt( mosfet->param[PHI] - VbsAux ) 
             - sqrt( mosfet->param[PHI] ) );
    
    return Vt;
}

// Calcular capacitancias do modelo MOS 
int mosfetCalcCapacitances( element* mosfet ) {
    int state = STATE_NOT_FOUND;
    double mi;
    if ( mosfet->param[POLARITY] == NMOS ) {
        mi = 0.05;
    } else { //PMOS
        mi = 0.02;
    }
    double Cox = (2 * mosfet->param[K_PARAM])/mi;
    
    if ( mosfet->param[STATE] == POE_CUT ) {
        mosfet->param[CGS] = Cox * mosfet->param[WIDTH] * mosfet->param[LD];
        mosfet->param[CGD] = Cox * mosfet->param[WIDTH] * mosfet->param[LD];
        mosfet->param[CGB] = Cox * mosfet->param[WIDTH] * mosfet->param[LENGTH];
        state = 0;
    }
    if ( mosfet->param[STATE] == POE_TRI ) {
        mosfet->param[CGS] =((Cox * mosfet->param[WIDTH] * mosfet->param[LD]) + 
                ((1.0/2.0) * Cox * mosfet->param[WIDTH] * mosfet->param[LENGTH]));
        mosfet->param[CGD] =((Cox * mosfet->param[WIDTH] * mosfet->param[LD]) + 
                ((1.0/2.0) * Cox * mosfet->param[WIDTH] * mosfet->param[LENGTH]));
        mosfet->param[CGB] = 0;
        state = 0;
    }
    if ( mosfet->param[STATE] == POE_SAT ) {
        mosfet->param[CGS] =(
                (Cox * mosfet->param[WIDTH] * mosfet->param[LD]) +
                ((2.0/3.0) * Cox * mosfet->param[WIDTH] * mosfet->param[LENGTH]));
        mosfet->param[CGD] =Cox * mosfet->param[WIDTH] * mosfet->param[LD];
        mosfet->param[CGB] = 0;
        state = 0;
    }
    return state;
}

double mosfetGetId( element* mosfet ) {
    double Vgs, Vt, VgsVt, Vds, bodyFx, Id;
    
    if ( mosfet->param[STATE] == POE_CUT ) {
       return MOS_CUTOFF_CURRENT;
    } 
    
    Vgs = mosfetGetVgs(mosfet);
    Vt = mosfetGetVt(mosfet);
    if ( mosfet->param[POLARITY] == NMOS ) {
        VgsVt = Vgs - Vt;
        Vds = mosfetGetVds(mosfet);
    }
    else {
        VgsVt = (-Vgs) - Vt;
        Vds = (-mosfetGetVds(mosfet));
    }
    bodyFx = 1 + (mosfet->param[LAMBDA] * Vds); //Fator do efeito de corpo
    
    if ( mosfet->param[STATE] == POE_TRI ) {
        Id = (mosfet->param[KWL] * ((2 * VgsVt * Vds) - (Vds * Vds)) * bodyFx); 
    } 
    if ( mosfet->param[STATE] == POE_SAT ) {
        Id = ( mosfet->param[KWL] * ( VgsVt * VgsVt ) * bodyFx);
    }
    if ( mosfet->param[POLARITY] == PMOS ) {
        return -Id;
    }
    return Id;
}

void mosfetReverseSourceDrain( element* mosfet ) {
    int auxConnection;
    double auxVoltage;
    
    auxVoltage = mosfet->param[V_SOURCE];
    mosfet->param[V_SOURCE] = mosfet->param[V_DRAIN];
    mosfet->param[V_DRAIN] = auxVoltage;
    
    auxConnection = mosfet->connection[POINT_SOURCE];
    mosfet->connection[POINT_SOURCE] = mosfet->connection[POINT_DRAIN];
    mosfet->connection[POINT_DRAIN] = auxConnection;
    
}

void mosfetUpdatePolarity( element* mosfet ) {
    if ((( mosfet->param[POLARITY] == NMOS ) && ( mosfet->param[V_SOURCE] > mosfet->param[V_DRAIN] )) ||
       (( mosfet->param[POLARITY] == PMOS ) && ( mosfet->param[V_SOURCE] < mosfet->param[V_DRAIN] ))) {
            mosfetReverseSourceDrain( mosfet );
    }
}

void mosfetUpdateState( element* mosfet ) {
    double Vds, Vgs, Vbs,Vt;
    double VgsVt;
    
    // Verifica a polarização
    mosfetUpdatePolarity( mosfet );
    
    Vds = mosfetGetVds( mosfet );
    Vgs = mosfetGetVgs( mosfet );
    Vbs = mosfetGetVbs( mosfet );
    Vt = mosfetGetVt( mosfet );
    if ( mosfet->param[POLARITY] == PMOS )
        VgsVt = (-Vgs) - Vt;
    else
        VgsVt = Vgs - Vt;
    
    if ( ( mosfet->param[POLARITY] == NMOS && ( Vgs < Vt ) ) ||
         ( mosfet->param[POLARITY] == PMOS && ( Vgs > (-Vt) ) )  
        ) {
        mosfet->param[STATE] = POE_CUT;
        return;
    }
    if ( ( mosfet->param[POLARITY] == NMOS && ( Vds <= VgsVt ) ) ||
         ( mosfet->param[POLARITY] == PMOS && ( (-Vds) <= VgsVt ) )
        ){
        mosfet->param[STATE] = POE_TRI;
        return;
    }
    mosfet->param[STATE] = POE_SAT;
    mosfetCalcCapacitances(mosfet);
    return;
}
