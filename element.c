#include <stdio.h>
#include "error.h"
#include "mosfet.h"
#include "messages.h"
#include "element.h"

void printElement(element* elmt){
    char *polarity;
    char line[MAX_LINE_SIZE+1];
    switch( elmt->type ) {
            case 'R': // Resistor
                sprintf(line, "Resistor %s %d %d %g",elmt->name,
                        elmt->connection[POINT_A],elmt->connection[POINT_B],
                        elmt->param[VALUE]);
                break;
            case 'L':
                sprintf(line,"Indutor  %s %d %d %g",elmt->name,
                        elmt->connection[POINT_A],elmt->connection[POINT_B],
                        elmt->param[VALUE]);
                break;
            case 'C':
                sprintf(line,"Capacitor %s %d %d %g",elmt->name,
                        elmt->connection[POINT_A],elmt->connection[POINT_B],
                        elmt->param[VALUE]);
                break;
            case 'K': 
                sprintf(line,"Acoplamento de indutores %s %g",elmt->name, elmt->param[VALUE]);
                break;
            case 'I': // Fonte de Corrente
                sprintf(line,"F.Corrente %s %d %d %g %g %g",elmt->name,
                        elmt->connection[POINT_A],elmt->connection[POINT_B],
                        elmt->param[AMPLITUDE], elmt->param[PHASE],
                        elmt->param[VALUE]);
                break;
            case 'V': // Fonte de Tensão
                sprintf(line,"F.Tensao %s %d %d %g %g %g",elmt->name,
                        elmt->connection[POINT_A],elmt->connection[POINT_B],
                        elmt->param[AMPLITUDE], elmt->param[PHASE],
                        elmt->param[VALUE]);
                break;
            case 'G':
                sprintf(line,"FCcT %s %d %d %d %d %g",elmt->name,
                        elmt->connection[POINT_A],elmt->connection[POINT_B],
                        elmt->connection[POINT_C],elmt->connection[POINT_D],
                        elmt->param[VALUE]
                );
                break;
            case 'E':
                sprintf(line,"FTcT %s %d %d %d %d %g",elmt->name,
                        elmt->connection[POINT_A],elmt->connection[POINT_B],
                        elmt->connection[POINT_C],elmt->connection[POINT_D],
                        elmt->param[VALUE]
                );
                break;
            case 'F':
                sprintf(line,"FCcC %s %d %d %d %d %g",elmt->name,
                        elmt->connection[POINT_A],elmt->connection[POINT_B],
                        elmt->connection[POINT_C],elmt->connection[POINT_D],
                        elmt->param[VALUE]
                );
                break;
            case 'H':
                sprintf(line,"FTcT %s %d %d %d %d %g",elmt->name,
                        elmt->connection[POINT_A],elmt->connection[POINT_B],
                        elmt->connection[POINT_C],elmt->connection[POINT_D],
                        elmt->param[VALUE]
                );
                break;
            case 'O':
                sprintf(line,"OpAmp    %s %d %d %d %d", elmt->name,
                        elmt->connection[POINT_A],elmt->connection[POINT_B],
                        elmt->connection[POINT_C],elmt->connection[POINT_D]
                );
                break;
        case 'M':
            //Transistor MOS: M<nome> <nód> <nóg> <nós> <nób> <NMOS ou PMOS> 
            //L=<comprimento> W=<largura> <K> <Vt0> <lambda> <gamma> <phi> <Ld>
            if ( elmt->param[POLARITY] == NMOS ) {
                polarity = NMOS_STR;
            } else if ( elmt->param[POLARITY] == PMOS ) {
                polarity = PMOS_STR;
            }
            sprintf(line,"MOSFET   %s %d %d %d %d %s L=%g W=%g %g %g %g %g %g %g", elmt->name,
                    elmt->connection[POINT_DRAIN],elmt->connection[POINT_GATE],
                    elmt->connection[POINT_SOURCE],elmt->connection[POINT_BODY],
                    polarity, elmt->param[LENGTH], elmt->param[WIDTH], 
                    elmt->param[K_PARAM], elmt->param[VTO], elmt->param[LAMBDA],
                    elmt->param[GAMMA], elmt->param[PHI], elmt->param[LD]);
                break;
            default:
                sprintf(line,"??????");
                break;
        }
    consolePrint(line);
}

void printNetlist(element* netlist, int netlistSize) {
    for ( int i = 1; i < netlistSize; i++ ) {
        printElement(&netlist[i]);
    }
}