#
# Generated - do not edit!
#
# NOCDDL
#
CND_BASEDIR=`pwd`
CND_BUILDDIR=build
CND_DISTDIR=dist
# Debug configuration
CND_PLATFORM_Debug=GNU-MacOSX
CND_ARTIFACT_DIR_Debug=dist/Debug/GNU-MacOSX
CND_ARTIFACT_NAME_Debug=sim-ce2
CND_ARTIFACT_PATH_Debug=dist/Debug/GNU-MacOSX/sim-ce2
CND_PACKAGE_DIR_Debug=dist/Debug/GNU-MacOSX/package
CND_PACKAGE_NAME_Debug=sim-ce2.tar
CND_PACKAGE_PATH_Debug=dist/Debug/GNU-MacOSX/package/sim-ce2.tar
# Release configuration
CND_PLATFORM_Release=GNU-MacOSX
CND_ARTIFACT_DIR_Release=dist/Release/GNU-MacOSX
CND_ARTIFACT_NAME_Release=sim-ce2
CND_ARTIFACT_PATH_Release=dist/Release/GNU-MacOSX/sim-ce2
CND_PACKAGE_DIR_Release=dist/Release/GNU-MacOSX/package
CND_PACKAGE_NAME_Release=sim-ce2.tar
CND_PACKAGE_PATH_Release=dist/Release/GNU-MacOSX/package/sim-ce2.tar
# Debug-Windows configuration
CND_PLATFORM_Debug-Windows=Cygwin-Windows
CND_ARTIFACT_DIR_Debug-Windows=dist/Debug-Windows/Cygwin-Windows
CND_ARTIFACT_NAME_Debug-Windows=sim-ce2
CND_ARTIFACT_PATH_Debug-Windows=dist/Debug-Windows/Cygwin-Windows/sim-ce2
CND_PACKAGE_DIR_Debug-Windows=dist/Debug-Windows/Cygwin-Windows/package
CND_PACKAGE_NAME_Debug-Windows=sim-ce2.tar
CND_PACKAGE_PATH_Debug-Windows=dist/Debug-Windows/Cygwin-Windows/package/sim-ce2.tar
#
# include compiler specific variables
#
# dmake command
ROOT:sh = test -f nbproject/private/Makefile-variables.mk || \
	(mkdir -p nbproject/private && touch nbproject/private/Makefile-variables.mk)
#
# gmake command
.PHONY: $(shell test -f nbproject/private/Makefile-variables.mk || (mkdir -p nbproject/private && touch nbproject/private/Makefile-variables.mk))
#
include nbproject/private/Makefile-variables.mk
