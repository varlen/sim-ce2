#ifndef OPTIONS_H
#define OPTIONS_H

#ifdef __cplusplus
extern "C" {
#endif

    typedef enum {LIN,DEC,OCT} Scale;
    typedef struct SimulationConfig {
        Scale scale;
        int points;
        double begin, end;  // frequencies in Hz
    } SimulationConfig;
    

#ifdef __cplusplus
}
#endif

#endif /* OPTIONS_H */

