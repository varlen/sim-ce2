/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>

#include "error.h"
#include "messages.h"
#include "netlistParser.h"
#include "frequencyAnalysis.h"
#include "element.h"
#include "netlistParser.h"
#include "openfile.h"
#include "systemMatrix.h"
#include "options.h"
#include "mosfet.h"
#include "mosfetManager.h"

#define MAX_ITERATIONS_NEWTON_RAPHSON 50
#define MAX_TRIALS 5

int main(int argc, char** argv) {
    char filename[MAX_SIZE_FILENAME + 1];
    char strRasc[MAX_LINE_SIZE + 1];
    FILE* arquivo;
    SimulationConfig options;
    filename[0] = '\0';
    // Default options
    options.begin = 20;
    options.end = 2000000000;
    options.points = 10;
    options.scale = OCT;
    
    element netlist[MAX_ELEMENTS]; // elemento 0 não é usado
    double sistm[MAX_NODE+1][MAX_NODE+2];
    double lastDCSolution[MAX_NODE+1];
    int solutionStatus;  
    int newtonRaphsonFinished;
    int nrIter = 0;
    int trials;
    int systemStatus =0;
    int netlistSize;
    
    int mosfetQtd = 0;
    int mosfetIndexes[MAX_MOSFET];
    
    char nodeList[MAX_NODE+1][MAX_INT_NODE_NAME];//"lista" no original - lista de nos
    int variableCount = 0;
    int extraCurrents = 0; // Correntes adicionais a serem calculadas
    
    printHeader();
    
    // Verificar se o netlist foi passado como parametro
    if ( argc > 1 ) {
        arquivo = openFile( argv[1] );
        strcpy(filename, argv[1]);
        if ( !arquivo )
            return BAD_FILE;
    } else {
        do {
            promptFilename(filename);
            arquivo = openFile( filename );
        } while ( !arquivo ) ;
    }
    
    
    netlistSize = parseNetlist(netlist, arquivo, &variableCount, &mosfetQtd, mosfetIndexes, nodeList, &options); 
    fclose( arquivo );
    extraCurrents = setExtraCurrents( netlist, netlistSize, &variableCount, nodeList );
    if ( extraCurrents < 0 ) {
        consolePrint("Numero maximo de variaveis excedido");
        exit(MAX_NODE_REACHED);
    }
    variableCount = variableCount + extraCurrents;
    sprintf(strRasc, "%d elementos", netlistSize);
    consolePrint(strRasc);
    sprintf(strRasc,"%d variaveis:", variableCount);
    consolePrint(strRasc);
    
    // Variaveis internas
    for ( int i = 0; i < variableCount + 1; i++ ) {
        consolePrint(nodeList[i]);
        lastDCSolution[i] = 0;
    }
    printNetlist(netlist, netlistSize);
    trials = 0;
    do {
        nrIter = 0;
        for ( int i = 0; i < variableCount + 1; i++ ) {
            lastDCSolution[i] = 0;
        }
        // Iterar para obter a solução por Newton-Raphson
        // Adicionar estampas dos elementos em DC
        while( !newtonRaphsonFinished && ( nrIter < MAX_ITERATIONS_NEWTON_RAPHSON ) && !systemStatus ) {
            sprintf(strRasc, "[Newton-Raphson]Iteracao #%d(Tentativa #%d)", nrIter, trials );
            consolePrint(strRasc);
            matrixInitialize(sistm);
            mosfetSetPolarities( netlist, mosfetIndexes, mosfetQtd );
            mosfetSetStates(netlist, mosfetIndexes, mosfetQtd);
            mosfetSetCapacitances(netlist, mosfetIndexes, mosfetQtd);
            mosfetPrintState(netlist, mosfetIndexes, mosfetQtd);
            matrixAddDCStamps(sistm, netlist, netlistSize, variableCount );
            
            //matrixPrint(sistm, variableCount);
            
            systemStatus = matrixSolveDC(sistm, variableCount);
            // Verificar convergencia
            mosfetStoreNRStep(sistm, netlist, variableCount, mosfetIndexes, mosfetQtd );
           
            newtonRaphsonFinished = matrixCheckNewtonRaphsonConvergence(sistm, lastDCSolution, variableCount );
//            matrixPrintDCSolution(sistm,nodeList,variableCount);
            // Armazenar solucao do passo atual
            copySolution(sistm, lastDCSolution, variableCount );
            nrIter++;
        }
        trials++;
        if ( nrIter == MAX_ITERATIONS_NEWTON_RAPHSON ) {
            sprintf(strRasc, "[Newton-Raphson] Maximo de iteracoes atingidas (%d)", nrIter );
            consolePrint(strRasc);
            
        }
        if ( systemStatus == SINGULAR_SYSTEM ) {
            consolePrint("Sistema singular");
        }
        if ( nrIter != MAX_ITERATIONS_NEWTON_RAPHSON && systemStatus != SINGULAR_SYSTEM ) {
            consolePrint("Solucao encontrada");
            mosfetPrintState(netlist, mosfetIndexes, mosfetQtd);
            solutionStatus = 1;
            consolePrint("------");
            matrixPrint(sistm, variableCount);
            matrixPrintDCSolution(sistm, nodeList, variableCount );
            break;
        }
        consolePrint("Solucao DC nao encontrada, escolhendo valores iniciais aleatorios");  
        mosfetRandomizeInitialConditions(netlist, mosfetIndexes, mosfetQtd);
    } 
    while ( !solutionStatus && trials < MAX_TRIALS );
    
    if ( trials == MAX_TRIALS ) {
        consolePrint("5 tentativas falhas para encontrar solucao DC");
    } else {
        consolePrint("Ponto de operação encontrado");
        mosfetPrintState(netlist, mosfetIndexes, mosfetQtd);
        //matrixPrint(sistm, variableCount);
        getchar();
        freqAnalysis(sistm,variableCount,netlist,nodeList,netlistSize,options,filename);
    }

    printf("\nPressione qualquer tecla para continuar...\n");
    getchar();
    return (EXIT_SUCCESS);
}

