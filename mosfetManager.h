#include "mosfet.h"

#ifndef MOSFETMANAGER_H
#define MOSFETMANAGER_H

#ifdef __cplusplus
extern "C" {
#endif

#define MAX_MOSFET 1209
#define STATE_OVERFLOW 1
    
    /**
     * Sets the states for each mosfet considering the voltages on its pins
     * @param netlist
     * @param mosfetIndexes
     * @param mosfetQtd
     */
    void mosfetSetStates( element* netlist, int mosfetIndexes[MAX_MOSFET], int mosfetQtd);
    void mosfetSetPolarities( element* netlist, int mosfetIndexes[MAX_MOSFET], int mosfetQtd );
    void mosfetSetCapacitances( element* netlist, int mosfetIndexes[MAX_MOSFET], int mosfetQtd );
    /**
     * Stores the state of MOSFET after the given step
     * @param matrix
     * @param netlist
     * @param variableCount
     * @param mosfetIndexes
     * @param mosfetQtd
     * @return 
     */
    int mosfetStoreNRStep(double matrix[][MAX_NODE+2], element* netlist, int variableCount, int mosfetIndexes[], int mosfetQtd); 
    /**
     * Randomize the mosfets' initial voltages 
     * @param netlist
     * @param mosfetIndexes
     * @param mosfetQtd
     */
    void mosfetRandomizeInitialConditions(element* netlist, int mosfetIndexes[MAX_MOSFET], int mosfetQtd);
    /**
     * Prints the voltages in all the mosfets
     * @param netlist
     * @param mosfetIndexes
     * @param mosfetQtd
     */
    void mosfetPrintState( element* netlist, int mosfetIndexes[],int mosfetQtd );
    /**
     * Prints the voltages and parameters for the given mosfet
     * @param mosfet pointer to the mosfet element
     */
    void mosfetDebug(element* mosfet);
#ifdef __cplusplus
}
#endif

#endif /* MOSFETMANAGER_H */

