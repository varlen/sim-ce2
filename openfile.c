#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>

#include "openfile.h"
#include "messages.h"

FILE* openFile(char* filename) {
    FILE* arquivo = fopen( filename ,"r");
    char line[MAX_LINE_SIZE];
    if ( !arquivo ) {
        sprintf(line,"%s", strerror( errno ) );
        consolePrint(line);
    }
    return arquivo;
}
