#include <stdio.h>
#include "messages.h"

int promptFilename( char* filename ) {
    printf("Digite o nome do arquivo:\n");
    scanf("%50s", filename );
    getchar();
    return 0;
}

// Para facilitar. Quando fizermos a GUI vai ser só mudar essa função.
int consolePrint( char* str ) {
    printf("%s\n", str );
    return 0;
}

void printHeader() {
    printf("Circuitos Eletricos II - 2016.1\n");
    printf("Prof Antonio Carlos Moreirao de Queiroz\n");
    printf("Analise na Frequencia de Circuitos com MOSFET\n");
    printf("Alunos: Arthur Petito, Luis Octavio e Varlen Neto\n\n");
}