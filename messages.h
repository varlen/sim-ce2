/* 
 * File:   messages.h
 * Author: varlen
 *
 * Created on 22 de Junho de 2016, 12:43
 */

#ifndef MESSAGES_H
#define MESSAGES_H

#define INVALID_FILENAME   1
#define VALID_FILENAME     0
#define MAX_LINE_SIZE      1000
#define MAX_SIZE_FILENAME 100

#ifdef __cplusplus
extern "C" {
#endif

    int promptFilename(char* filename);
    int consolePrint(char* name);
    void printHeader();

#ifdef __cplusplus
}
#endif

#endif /* MESSAGES_H */

