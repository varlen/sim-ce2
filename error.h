/* 
 * File:   error.h
 * Author: varlen
 *
 * Created on 23 de Junho de 2016, 16:36
 */

#ifndef ERROR_H
#define ERROR_H

#ifdef __cplusplus
extern "C" {
#endif

#define BAD_FILE                1
#define NETLIST_TOO_BIG         2
#define MAX_POLY_TERMS_REACHED  3
#define SINGULAR_SYSTEM         4
#define FEW_INDUCTORS           5
#define BAD_MOS                 6
#define BAD_INDUCTOR            7
#define STATE_NOT_FOUND         8
#define NAN_EXCEPT              10
#define UNKNOWN_ELEMENT         -1
#define MAX_NODE_REACHED        -2
#define ELEMENT_NOT_FOUND       -3






#ifdef __cplusplus
}
#endif

#endif /* ERROR_H */

