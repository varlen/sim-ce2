#ifndef NETLISTPARSER_H
#define NETLISTPARSER_H

#define MAX_ELEMENTS  50
#define MAX_NODE 50
#define MAX_NODE_NAME 10
// Internal node name, bigger, to fit jx in front of certain node names:
#define MAX_INT_NODE_NAME MAX_NODE_NAME+2
#define MAX_NETLIST_LINE 80

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <ctype.h>

#include "element.h"
#include "options.h"
#include "mosfetManager.h"

#ifdef __cplusplus
extern "C" {
#endif

    int matchToNode(char* nodeName, char nodeList[][MAX_INT_NODE_NAME], int* variableCount);
    // Retorna o numero de elementos no netlist
    int parseNetlist(element* netlist, FILE* arquivo, int* variableCount, int* mosfetCount, int mosfetIndex[MAX_MOSFET], char nodeList[][MAX_INT_NODE_NAME], SimulationConfig* options );
    /**
     * Adiciona as correntes extras a serem calculadas e as respectivas conexões nos pontos x e y dos elementos
     * 
     * @param netlist
     * @param netlistSize
     * @param variableCount
     * @param nodeList
     * @return int Numero de correntes adicionadas
     */
    int setExtraCurrents(element* netlist, int netlistSize, int* variableCount, char nodeList[][MAX_INT_NODE_NAME]);
    
    /**
     * Procura um elemento no netlist
     * @param netlist
     * @param netlistSize
     * @param nodeName
     * @return int Indice do elemento
     */
    int findElementName (element* netlist, int netlistSize, char* elementName);
    
    /**
     * Conta quantos elementos de certo tipo existem no netlist
     * @param netlist
     * @param netlistSize
     * @param elementType
     * @return int Numero de elementos do tipo especificado
     */
    int countElementType (element* netlist, int netlistSize, char elementType);

#ifdef __cplusplus
}
#endif

#endif /* NETLISTPARSER_H */

