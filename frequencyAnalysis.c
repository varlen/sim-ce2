#include "frequencyAnalysis.h"
#include "systemMatrix.h"
#include "messages.h"
#include "mosfet.h"
#include "options.h"
#include "error.h"

void freqAnalysis( double matrix[][MAX_NODE+2], int variableCount, element* netlist, char nodeList[][MAX_INT_NODE_NAME], int netlistSize, SimulationConfig options, char filename[MAX_SIZE_FILENAME + 1]) {
    char strBuf[MAX_SIZE_FILENAME+1];
    char msgBuf[MAX_LINE_SIZE+1];
    double complex acSys[MAX_NODE+2][MAX_NODE+2];
    double freq, frequency[MAX_FREQ];
    FILE* outputFile;
    int numOfFreq = 0;
    strBuf[0] = '\0';
    //acmatrixInitialize(acSys);
    //acmatrixPrint(acSys, variableCount);
    // Calculate steps 
    generateOutputFileName(filename,strBuf);
    outputFile = fopen(strBuf, "w");
    addOutputFileHeader(outputFile, nodeList, variableCount);
    numOfFreq = generateFrequencies(frequency,options);
    printf("Iniciando analise AC\n");
    for (int findex = 0; findex < numOfFreq; findex++) {
        //sprintf(msgBuf, "Calculando sistema para f=%lg", frequency[findex]);
        //consolePrint(msgBuf);
        acmatrixInitialize(acSys);
        freq = frequency[findex] * (2 * M_PI); // Hz -> rad/s
        acmatrixAddStamps(acSys, netlistSize, netlist, variableCount, freq);
        acmatrixSolve(acSys,variableCount);
//        acmatrixPrint(acSys,variableCount);
        acStoreResults(outputFile,frequency[findex],acSys,variableCount);
    }
    fclose(outputFile);
    sprintf(msgBuf,"Analise completa com %d pontos", numOfFreq);
    consolePrint(msgBuf);
    sprintf(msgBuf,"Resultado armazenado em %s", strBuf);
    consolePrint(msgBuf);
}

void generateOutputFileName(char inputName[MAX_SIZE_FILENAME + 1], char strBuf[MAX_SIZE_FILENAME + 1]) {
    int eolPosition;
    strcpy(strBuf, inputName);
    for (eolPosition = 0; eolPosition < MAX_SIZE_FILENAME; eolPosition++ ) {
        if (strBuf[eolPosition] == '\0') break;
    }
    for ( int i = eolPosition; i > 0; i-- ) {
        if (strBuf[i] == '.') {
            strBuf[i] = '\0';
            break;
        }
    }
    strcat(strBuf, ".tab");
}

void acStoreResults(FILE* outputFile, double freq, double complex acSys[][MAX_NODE+2], int variableCount) {
    fprintf(outputFile, "%lg ", freq);
    double modulo, fase;
    for (int i = 0; i < variableCount; i++ ) {
        modulo = cabs(acSys[i][variableCount+1]);
        fase = carg(acSys[i][variableCount+1]) * (180.0 / M_PI); // rad -> grau
        if ( modulo < TOLG ) modulo = TOLG;
        fprintf(outputFile, "%lg %lg ", modulo, fase);
    }
    fprintf(outputFile, "\n");
}

void addOutputFileHeader(FILE* outputFile, char nodeList[][MAX_INT_NODE_NAME], int variableCount ) {
    fprintf(outputFile, "f ");
    for (int i = 0; i < variableCount; i++ ) {
        fprintf(outputFile, "%sm %sf ", nodeList[i], nodeList[i]);
    }
    fprintf(outputFile,"\n");
}

int generateFrequencies(double frequency[MAX_FREQ], SimulationConfig options ) {
    double freq, increase, nxt, first, last;
    int count = 0, totalCount = 0; // count -> number of points locally, totalCount -> total number of points
    if (!options.points)
        options.points = 1;
    switch(options.scale) {
        case OCT:
            count = 0;
            first = exp(options.begin);
            freq = options.begin;
            while ( freq < options.end ) {
                first = log(freq);
                last = log(freq*2);
                increase = (last - first)/(options.points - 1);
                for (double expfreq = first; expfreq <= last; expfreq += increase) {
                    freq = exp(expfreq);
                    if (freq > options.end) {
                        break;
                    }
                    frequency[count] = freq;
                    count++;
                }
                if (freq > options.end) {
                    break;
                }
            }
            return count;
        case LIN:
            freq = options.begin;
            increase = (options.end - options.begin )/( options.points - 1 );
            count = 0;
            while( freq < options.end && count + 1 < MAX_FREQ ) {
                frequency[count] = freq;
                freq = freq + increase;
                count++;
            }
            frequency[count] = options.end;
            return count + 1;
        case DEC:
            count = 0;
            first = exp(options.begin);
            freq = options.begin;
            while ( freq < options.end ) {
                first = log(freq);
                last = log(freq*10);
                increase = (last - first)/(options.points - 1);
                for (double expfreq = first; expfreq <= last; expfreq += increase) {
                    freq = exp(expfreq);
                    if (freq > options.end) {
                        break;
                    }
                    frequency[count] = freq;
                    count++;
                }
                if (freq > options.end) {
                    break;
                }
            }
            return count;
    }
    return 0;
}

void acmatrixInitialize(double complex acmatrix[][MAX_NODE+2]) {
    for ( int i = 0; i < MAX_NODE; i++ ) {
        for (int j = 0; j < MAX_NODE; j++) {
            acmatrix[i][j] = 0 + I*0;
        }
    }
}

void acmatrixPrint(double complex acmatrix[][MAX_NODE+2], int variableCount) {
    char line[MAX_LINE_SIZE];
    char valueStr[50];
    for ( int i = 1; i <= variableCount; i++ ) {
        line[0] = '\0';
        for (int j = 1; j <= variableCount + 1; j++) {
            if (creal(acmatrix[i][j]) != 0) 
                sprintf(valueStr, "%+.2e%+.2ej", creal(acmatrix[i][j]), cimag(acmatrix[i][j]));
            else 
                strcpy(valueStr,".........");
            strcat(line,valueStr);
            if ( j != variableCount + 1 ) {
                strcat(line," ");
            }
        }
        consolePrint(line);
    }
}

int acmatrixSolve(double complex acmatrix[][MAX_NODE+2], int variableCount){
    int i,j,l,a;
    double complex t,p;
    
    for (i=1; i <= variableCount; i++ ) {
        t=0.0;
        a=i;
        
        for (l=i; l<= variableCount; l++) {
            if( cabs(acmatrix[l][i]) > cabs(t) ) {
                a = l;
                t = acmatrix[l][i];
            }
        }
        
        if ( i != a ) {
            for ( l=1;l<= variableCount + 1; l++ ) {
                p = acmatrix[i][l];
                acmatrix[i][l] = acmatrix[a][l];
                acmatrix[a][l] = p;
            }
        }
        
        if ( cabs(t) < TOLG ) {
            return SINGULAR_SYSTEM;
        }
        
        for (j= variableCount+1; j>0; j--) {
            acmatrix[i][j]/= t;
            p = acmatrix[i][j];
            if ( p != 0 ) {
                for(l=1;l<= variableCount; l++ ) {
                    if ( l != i ) 
                        acmatrix[l][j] -= acmatrix[l][i] * p;
                }
            }            
        }
    }
    return 0;
}

void acmatrixAddStamps(double complex matrix[][MAX_NODE+2], int netlistSize, element* netlist, int variableCount, double angularFreq ) {
    double complex g;
    char strRasc[MAX_LINE_SIZE];
    for ( int elementIndex = 1; elementIndex <= netlistSize; elementIndex++ ) {
        switch( netlist[elementIndex].type ) {
            case 'R':
                g = 1/netlist[elementIndex].param[VALUE];
                matrix[netlist[elementIndex].connection[POINT_A]]
                      [netlist[elementIndex].connection[POINT_A]] += g;
                matrix[netlist[elementIndex].connection[POINT_B]]
                      [netlist[elementIndex].connection[POINT_B]] += g;
                matrix[netlist[elementIndex].connection[POINT_A]]
                      [netlist[elementIndex].connection[POINT_B]] -= g;
                matrix[netlist[elementIndex].connection[POINT_B]]
                      [netlist[elementIndex].connection[POINT_A]] -= g;
                break;
            case 'C': 
                g = I * angularFreq * netlist[elementIndex].param[VALUE];
                matrix[netlist[elementIndex].connection[POINT_A]]
                      [netlist[elementIndex].connection[POINT_A]] += g;
                matrix[netlist[elementIndex].connection[POINT_B]]
                      [netlist[elementIndex].connection[POINT_B]] += g;
                matrix[netlist[elementIndex].connection[POINT_A]]
                      [netlist[elementIndex].connection[POINT_B]] -= g;
                matrix[netlist[elementIndex].connection[POINT_B]]
                      [netlist[elementIndex].connection[POINT_A]] -= g;
                break;
            case 'L': 
                g = (I * angularFreq * netlist[elementIndex].param[VALUE]);
                matrix[netlist[elementIndex].connection[POINT_A]]
                      [netlist[elementIndex].connection[POINT_X]]
                    += 1;
                matrix[netlist[elementIndex].connection[POINT_B]]
                      [netlist[elementIndex].connection[POINT_X]]
                    -= 1;
                matrix[netlist[elementIndex].connection[POINT_X]]
                      [netlist[elementIndex].connection[POINT_A]]
                    -= 1;
                matrix[netlist[elementIndex].connection[POINT_X]]
                      [netlist[elementIndex].connection[POINT_B]]
                    += 1;
                matrix[netlist[elementIndex].connection[POINT_X]]
                      [netlist[elementIndex].connection[POINT_X]]
                    += g;
                break;
            case 'K':
                g = I * angularFreq * netlist[elementIndex].param[VALUE] *
                        sqrt( netlist[netlist[elementIndex].connection[INDUCTOR_A]].param[VALUE] *
                        netlist[netlist[elementIndex].connection[INDUCTOR_B]].param[VALUE] );
                netlist[elementIndex].connection[POINT_X] = netlist[netlist[elementIndex].connection[POINT_A]].connection[POINT_X]; 
                netlist[elementIndex].connection[POINT_Y] = netlist[netlist[elementIndex].connection[POINT_B]].connection[POINT_X]; 
                matrix[netlist[elementIndex].connection[POINT_X]]
                      [netlist[elementIndex].connection[POINT_Y]] += g;
                matrix[netlist[elementIndex].connection[POINT_Y]]
                      [netlist[elementIndex].connection[POINT_X]] += g;
                break;
            case 'M':
                mosfetAddStampAC( matrix, &netlist[elementIndex], variableCount,angularFreq );
                break;
            case 'G':
                matrix[netlist[elementIndex].connection[POINT_A]]
                      [netlist[elementIndex].connection[POINT_C]] 
                    += netlist[elementIndex].param[VALUE];
                matrix[netlist[elementIndex].connection[POINT_B]]
                      [netlist[elementIndex].connection[POINT_D]] 
                    += netlist[elementIndex].param[VALUE];
                
                matrix[netlist[elementIndex].connection[POINT_A]]
                      [netlist[elementIndex].connection[POINT_D]]
                    -= netlist[elementIndex].param[VALUE];
                matrix[netlist[elementIndex].connection[POINT_B]]
                      [netlist[elementIndex].connection[POINT_C]] 
                    -= netlist[elementIndex].param[VALUE];
                break;
                
            case 'I':
                g = 
                    ( netlist[elementIndex].param[AMPLITUDE] 
                      * cos(netlist[elementIndex].param[PHASE]*(180.0/M_PI))
                    ) 
                    + I * netlist[elementIndex].param[AMPLITUDE] 
                        * sin(netlist[elementIndex].param[PHASE]*(180.0/M_PI));
                
                matrix[netlist[elementIndex].connection[POINT_A]]
                      [variableCount + 1] 
                    -= netlist[elementIndex].param[VALUE];
                matrix[netlist[elementIndex].connection[POINT_B]]
                      [variableCount + 1] 
                    += g;
                break;
                
            case 'V':
                g = 
                    ( netlist[elementIndex].param[AMPLITUDE] 
                      * cos(netlist[elementIndex].param[PHASE]*(180.0/M_PI))
                    ) 
                    + I * netlist[elementIndex].param[AMPLITUDE] 
                        * sin(netlist[elementIndex].param[PHASE]*(180.0/M_PI));
                
                matrix[netlist[elementIndex].connection[POINT_A]]
                      [netlist[elementIndex].connection[POINT_X]]
                    += 1;
                matrix[netlist[elementIndex].connection[POINT_B]]
                      [netlist[elementIndex].connection[POINT_X]]
                    -= 1;
                matrix[netlist[elementIndex].connection[POINT_X]]
                      [netlist[elementIndex].connection[POINT_A]]
                    += 1;
                matrix[netlist[elementIndex].connection[POINT_X]]
                      [netlist[elementIndex].connection[POINT_B]]
                    -= 1;
                matrix[netlist[elementIndex].connection[POINT_X]]
                      [variableCount+1]
                    += g;
                break;
                
            case 'E':
                matrix[netlist[elementIndex].connection[POINT_A]]
                      [netlist[elementIndex].connection[POINT_X]]
                    += 1;
                matrix[netlist[elementIndex].connection[POINT_B]]
                      [netlist[elementIndex].connection[POINT_X]]
                    -= 1;
                matrix[netlist[elementIndex].connection[POINT_X]]
                      [netlist[elementIndex].connection[POINT_A]]
                    -= 1;
                matrix[netlist[elementIndex].connection[POINT_X]]
                      [netlist[elementIndex].connection[POINT_B]]
                    += 1;
                matrix[netlist[elementIndex].connection[POINT_X]]
                      [netlist[elementIndex].connection[POINT_C]]
                    += netlist[elementIndex].param[VALUE];
                matrix[netlist[elementIndex].connection[POINT_X]]
                      [netlist[elementIndex].connection[POINT_D]]
                    -= netlist[elementIndex].param[VALUE];
                break;
            case 'F':
                matrix[netlist[elementIndex].connection[POINT_A]]
                      [netlist[elementIndex].connection[POINT_X]]
                    += netlist[elementIndex].param[VALUE];
                matrix[netlist[elementIndex].connection[POINT_B]]
                      [netlist[elementIndex].connection[POINT_X]]
                    -= netlist[elementIndex].param[VALUE];
                
                matrix[netlist[elementIndex].connection[POINT_C]]
                      [netlist[elementIndex].connection[POINT_X]]
                    += 1;
                matrix[netlist[elementIndex].connection[POINT_D]]
                      [netlist[elementIndex].connection[POINT_X]]
                    -= 1;
                
                matrix[netlist[elementIndex].connection[POINT_X]]
                      [netlist[elementIndex].connection[POINT_C]]
                    -= 1;
                matrix[netlist[elementIndex].connection[POINT_X]]
                      [netlist[elementIndex].connection[POINT_D]]
                    += 1;
                break;
            case 'H':
                matrix[netlist[elementIndex].connection[POINT_A]]
                      [netlist[elementIndex].connection[POINT_Y]] 
                    += 1;
                matrix[netlist[elementIndex].connection[POINT_B]]
                      [netlist[elementIndex].connection[POINT_Y]] 
                    -= 1;
                matrix[netlist[elementIndex].connection[POINT_C]]
                      [netlist[elementIndex].connection[POINT_X]] 
                    += 1;
                matrix[netlist[elementIndex].connection[POINT_D]]
                      [netlist[elementIndex].connection[POINT_X]] 
                    -= 1;
                
                matrix[netlist[elementIndex].connection[POINT_Y]]
                      [netlist[elementIndex].connection[POINT_A]] 
                    -= 1;
                matrix[netlist[elementIndex].connection[POINT_Y]]
                      [netlist[elementIndex].connection[POINT_B]] 
                    += 1;
                matrix[netlist[elementIndex].connection[POINT_X]]
                      [netlist[elementIndex].connection[POINT_C]] 
                    -= 1;
                matrix[netlist[elementIndex].connection[POINT_X]]
                      [netlist[elementIndex].connection[POINT_D]] 
                    += 1;
                
                matrix[netlist[elementIndex].connection[POINT_X]]
                      [netlist[elementIndex].connection[POINT_Y]] 
                    += netlist[elementIndex].param[VALUE];
                break;
            case 'O':
                matrix[netlist[elementIndex].connection[POINT_A]]
                      [netlist[elementIndex].connection[POINT_X]] 
                    += 1;
                matrix[netlist[elementIndex].connection[POINT_B]]
                      [netlist[elementIndex].connection[POINT_X]] 
                    -= 1;
                matrix[netlist[elementIndex].connection[POINT_X]]
                      [netlist[elementIndex].connection[POINT_C]] 
                    += 1;
                matrix[netlist[elementIndex].connection[POINT_X]]
                      [netlist[elementIndex].connection[POINT_D]] 
                    -= 1;
                break;
            default:
                sprintf( strRasc, "Sem estampa AC para %c", netlist[elementIndex].type );
                consolePrint( strRasc );
        }
        //sprintf( strRasc, "Matriz apos elemento %s:", netlist[elementIndex].name );
        //consolePrint( strRasc );
        //acmatrixPrint(matrix, variableCount);
        //consolePrint("");
        //getchar();
    }
}