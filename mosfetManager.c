#include <stdlib.h>
#include <time.h>

#include "mosfetManager.h"
#include "mosfet.h"
#include "messages.h"

void mosfetSetStates( element* netlist, int mosfetIndexes[MAX_MOSFET], int mosfetQtd ) {
    for ( int i = 0; i < mosfetQtd; i++ ) {
        printf("Setting state %s ( Currently: %f )\n", netlist[mosfetIndexes[i]].name, netlist[mosfetIndexes[i]].param[STATE] );
        mosfetUpdateState( &netlist[mosfetIndexes[i]] );
    }
}

void mosfetSetPolarities( element* netlist, int mosfetIndexes[MAX_MOSFET], int mosfetQtd ) {
    for ( int i = 0; i < mosfetQtd; i++ ) {
        printf("Setting state %s ( Currently: %f )\n", netlist[mosfetIndexes[i]].name, netlist[mosfetIndexes[i]].param[STATE] );
        mosfetUpdatePolarity( &netlist[mosfetIndexes[i]] );
    }
}

void mosfetSetCapacitances( element* netlist, int mosfetIndexes[MAX_MOSFET], int mosfetQtd ) {
    for ( int i = 0; i < mosfetQtd; i++ ) {
        printf("Setting capacitances...\n");
        mosfetCalcCapacitances( &netlist[mosfetIndexes[i]] );
    }
}
int mosfetStoreNRStep(double matrix[][MAX_NODE+2], element* netlist, int variableCount, int mosfetIndexes[MAX_MOSFET], int mosfetQtd) {
    double vGate, vSource, vDrain, vBody;
    int i, connection;
    for ( i = 0; i < mosfetQtd; i++) {
            vGate = matrix[netlist[mosfetIndexes[i]].connection[POINT_GATE]][variableCount+1];
            vSource = matrix[netlist[mosfetIndexes[i]].connection[POINT_SOURCE]][variableCount+1];
            vDrain = matrix[netlist[mosfetIndexes[i]].connection[POINT_DRAIN]][variableCount+1];
            vBody = matrix[netlist[mosfetIndexes[i]].connection[POINT_BODY]][variableCount+1];
        
        netlist[mosfetIndexes[i]].param[I_DRAIN] = 0;
                
        netlist[mosfetIndexes[i]].param[V_GATE] = vGate;
        netlist[mosfetIndexes[i]].param[V_SOURCE] = vSource;
        netlist[mosfetIndexes[i]].param[V_DRAIN] = vDrain;
        netlist[mosfetIndexes[i]].param[V_BODY] = vBody;
        
        printf("Armazenando passo do nr*-> %s \t\t Vg[%d]:%e\tVs[%d]:%e\tVd[%d]:%e\tVb[%d]:%e;\t\n", netlist[mosfetIndexes[i]].name,
                netlist[mosfetIndexes[i]].connection[POINT_GATE],
                netlist[mosfetIndexes[i]].param[V_GATE],
                netlist[mosfetIndexes[i]].connection[POINT_SOURCE],
                netlist[mosfetIndexes[i]].param[V_SOURCE],
                netlist[mosfetIndexes[i]].connection[POINT_DRAIN],
                netlist[mosfetIndexes[i]].param[V_DRAIN],
                netlist[mosfetIndexes[i]].connection[POINT_BODY],
                netlist[mosfetIndexes[i]].param[V_BODY]);
    }
}

void mosfetRandomizeInitialConditions(element* netlist, int mosfetIndexes[], int mosfetQtd) {
    srand(time(0));
    double tmp = 0;
    for ( int i = 0; i < mosfetQtd; i++) {
        tmp = ( rand() % 10 ) * 0.1;
        netlist[mosfetIndexes[i]].param[V_GATE] = ( rand() % 10 ) * 0.1;
        netlist[mosfetIndexes[i]].param[V_SOURCE] = ( rand() % 10 ) * 0.1;
        netlist[mosfetIndexes[i]].param[V_DRAIN] = tmp;
        netlist[mosfetIndexes[i]].param[V_BODY] = tmp;
        mosfetUpdatePolarity(&netlist[mosfetIndexes[i]]);
    }
}

void mosfetPrintState( element* netlist, int mosfetIndexes[],int mosfetQtd ) {
    for ( int i = 0; i < mosfetQtd; i++ ) {  
        mosfetDebug(&netlist[mosfetIndexes[i]]);
    }
}

void mosfetDebug(element* mosfet) {
    char strRasc[MAX_LINE_SIZE];
    char stateStr[15];
    double Vt, Gm, Gdsn, Gmb, Id;
    if ( mosfet->param[STATE] == POE_CUT ) {
        strcpy(stateStr,"Corte");
    }
    if ( mosfet->param[STATE] == POE_SAT ) {
        strcpy(stateStr,"Saturação");
    }
    if ( mosfet->param[STATE] == POE_TRI) {
        strcpy(stateStr,"Triodo");
    }
    Vt = mosfetGetVt(mosfet);
    Gm = mosfetGetGm(mosfet);
    Gdsn = mosfetGetGdsn(mosfet);
    Gmb = mosfetGetGmb(mosfet);
    Id = mosfetGetId(mosfet);

    sprintf(strRasc, "%s %s (G:%+.2e\tD:%+.2e\tS:%+.2e\tB:%+.2e\tVt:%+.2e\tGm:%+.2e\tGdsn:%+.2e\tGmb:%+.2e\tId:%+.4e\tCgs:%.2e\tCgd:%.2e\tCbg:%.2e)", 
                        mosfet->name,
                        stateStr,
                        mosfet->param[V_GATE],
                        mosfet->param[V_DRAIN],
                        mosfet->param[V_SOURCE],
                        mosfet->param[V_BODY],
                        Vt,Gm,Gdsn,Gmb,Id,
                        mosfet->param[CGS],
                        mosfet->param[CGD],
                        mosfet->param[CGB]
            );
    consolePrint(strRasc);
}