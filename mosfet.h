#ifndef MOSFET_H
#define MOSFET_H
#include <math.h>

#include "element.h"
#include "netlistParser.h"
#include "mosfetManager.h"
#include "frequencyAnalysis.h"

#ifdef __cplusplus
extern "C" {
#endif

#define GDS_MIN 2e-9
    
// Connections
#define POINT_DRAIN  0 
#define POINT_GATE   1
#define POINT_SOURCE 2
#define POINT_BODY   3

// DC Parameters
#define MOS_CUTOFF_RESISTANCE 1e10
#define MOS_CUTOFF_CONDUTANCE 1e-10
#define MOS_CUTOFF_CURRENT    1e-9
    
// Model 1
#define RDS    1
#define GMVGS  2
#define GMVBS  3
#define I_ZERO 4

#define NMOS 0
#define PMOS 1
    
#define PMOS_STR  "PMOS"
#define NMOS_STR  "NMOS"

#define POLARITY    0
#define WIDTH       1
#define LENGTH      2
#define K_PARAM     3
#define VTO         4
#define LAMBDA      5
#define GAMMA       6
#define PHI         7
#define LD          8
#define KWL         15
#define CGS         16
#define CGD         17
#define CGB         18

#define STATE     9  // Região de operação do MOSFET )
//  0 - Desconhecido
//  1 - Corte
// -1 - Corte ( Polaridade inversa )
//  2 - Saturação
// -2 - Saturação ( Polaridade inversa )
//  3 - Triodo
// -3 - Triodo ( Polaridade inversa )
//  4 - Erro -> Todos os estados possiveis já foram testados

#define V_GATE        10
#define V_SOURCE      11
#define V_DRAIN       12
#define V_BODY        13
#define I_DRAIN       14
    
#define POE_UNKNOWN   0
#define POE_CUT       1
#define POE_SAT       2
#define POE_TRI       3
#define POE_ERR       4
    
    
    void mosfetAddStampDC( double matrix[][MAX_NODE+2], element* mosfet , int variableCount);
    void mosfetAddStampAC( double complex matrix[][MAX_NODE+2], element* mosfet, int variableCount, double angularFreq );
    
    // CutDc -> Especifica para o corte
    void mosfetAddStampCutDC( double matrix[][MAX_NODE+2], element* mosfet , int variableCount);
    // FullDc -> Saturacao e Triodo
    void mosfetAddStampFullDC( double matrix[][MAX_NODE+2], element* mosfet , int variableCount);
    
    void mosfetUpdatePolarity( element* mosfet );
    void mosfetReverseSourceDrain( element* mosfet );
    /**
     * Returns the number of MOSFETs in the given netlist
     * @param netlist
     * @param netlistSize
     * @return Number of mosfets
     */
    int mosfetCount( element* netlist, int netlistSize);
    
    /**
     * Validates the polarity of MOSFET to check if D and S are really D and S.
     * @param matrix
     * @param variableCount
     * @param mosfet
     * @return 0 if valid, 1 if invalid
     */
    double mosfetGetVt(element* mosfet);
    double mosfetGetGmb(element* mosfet);
    double mosfetGetGm(element* mosfet);
    double mosfetGetGdsn(element* mosfet);
    double mosfetGetVgs(element* mosfet);
    double mosfetGetVds(element* mosfet);
    double mosfetGetVbs(element* mosfet);
    double mosfetGetId(element* mosfet);
    
    /**
     * Calculates the capacitances for MOS' model
     * @param mosfet
     * @return 0 (OK) or 8 (STATE_NOT_FOUND)
     */
    int mosfetCalcCapacitances( element* mosfet );
        
    /**
     * Updates the operation point for the mosfet, considering its voltages
     * @param mosfet
     */
    void mosfetUpdateState( element* mosfet );
    
#ifdef __cplusplus
}
#endif

#endif /* MOSFET_H */

