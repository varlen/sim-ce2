/*
 *  Definicao generica de um elemento do netlist
 */

#ifndef ELEMENTO_H
#define ELEMENTO_H

#define MAX_STR_ELEMENT 12
#define MAX_NUM_PARAM   19

#define VALUE           0
#define AMPLITUDE       1
#define PHASE           2

#define POINT_A         0
#define POINT_B         1
#define POINT_C         2
#define POINT_D         3
#define POINT_X         4
#define POINT_Y         5

#define INDUCTOR_A      0
#define INDUCTOR_B      1
#define K_VALUE         2



#ifdef __cplusplus
extern "C" {
#endif

    typedef struct element {
        char name[MAX_STR_ELEMENT], type;
        int connection[6];
        double param[MAX_NUM_PARAM];
    } element;

    void printElement(element*);
    /**
     * Outputs the array of elements, one element by line
     * @param netlist
     * @param netlistSize
     * @return void
     */
    void printNetlist(element* netlist, int netlistSize);

#ifdef __cplusplus
}
#endif

#endif /* ELEMENTO_H */

